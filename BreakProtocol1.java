import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class BreakProtocol1{
	static int portNum = 11337;
	static String hostname = "localhost";

	public static void main(String args[]) throws Exception{
		// Setup the connection to the server
		Socket serverConn = new Socket(hostname, portNum);
		InputStream readingStream = serverConn.getInputStream();
		OutputStream writingStream = serverConn.getOutputStream();

		// Read Alice's session key in from the session key file
		FileInputStream aliceKeyFile = new FileInputStream("Protocol1.session-key.enc");
		byte[] aliceKey = new byte[32];
		aliceKeyFile.read(aliceKey);

		// Send Alice's session key to the server
		writingStream.write(aliceKey);

		// Receive the server's response
		byte[] bobNonce = new byte[16];
		byte[] bobEncNonce = new byte[32];
		byte[] bobSecretMessage = new byte[64];
		readingStream.read(bobNonce);
		readingStream.read(bobEncNonce);
		readingStream.read(bobSecretMessage);

		// Use Bob's encrypted nonce as the new session key
		serverConn = new Socket(hostname, portNum);
		readingStream = serverConn.getInputStream();
		writingStream = serverConn.getOutputStream();
		byte[] malloryKey = bobNonce;
		byte[] malloryEncNonce = bobEncNonce;
		writingStream.write(malloryEncNonce);

		// Receive the response
		byte[] bobNonce2 = new byte[16];
		byte[] bobEncNonce2 = new byte[32];
		byte[] bobSecretMessage2 = new byte[64];
		readingStream.read(bobNonce2);
		readingStream.read(bobEncNonce2);
		readingStream.read(bobSecretMessage2);

		// Decrypt the secret message
		Key aesKey = new SecretKeySpec(malloryKey, "AES");
		Cipher decAEScipher = Cipher.getInstance("AES");
		decAEScipher.init(Cipher.DECRYPT_MODE, aesKey);
		byte[] messageBytes = decAEScipher.doFinal(bobSecretMessage2);

		// Print out the secret message
		System.out.println(new String(messageBytes));
	}
}
